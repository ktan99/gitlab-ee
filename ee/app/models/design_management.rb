# frozen_string_literal: true

module DesignManagement
  def self.table_name_prefix
    'design_management_'
  end
end
